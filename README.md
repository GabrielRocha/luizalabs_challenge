
## Challenge

#### Deliverables 
"Luizalabs Employee Manager" app must have:
* A Django Admin panel to manage employees' data
* An API to list, add and remove employees

**Response**
```
[
    {
        "name": "Arnaldo Pereira",
        "email": "arnaldo@luizalabs.com",
        "department": "Architecture"
    },
    {
        "name": "Renato Pedigoni",
        "email": "renato@luizalabs.com",
        "department": "E-commerce"
    },
    {
        "name": "Thiago Catoto",
        "email": "catoto@luizalabs.com",
        "department": "Mobile"
    }
]
```

# Environments

## Development

Create .env file
```
cp .env_example .env
```

Edit the **.env** and insert the values for variable below:

* SECRET_KEY
* POSTGRES_PASSWORD
* POSTGRES_USER
* POSTGRES_DB

This project use [docker]("https://docs.docker.com/") for isolate the libraries and packages.

### How to Install

**Docker**
https://docs.docker.com/engine/installation/

**Docker Compose**
https://docs.docker.com/compose/install/

### How to start project

**Build** docker image
```
docker-compose build
```

**Tests**
```
docker-compose run django_api test
```

**Super User**
```
docker-compose run django_api createsuperuser
```

**Run** the local development server
```
docker-compose up
```

# Features

### Routes

**Create the employee**
```
curl -X POST -H "Content-Type: application/json" -d '{"name":"user","email":"user@luizalab.com","department":"Development"}' http://localhost:8000/employee/
```

**List all employees**
```
curl -X GET http://localhost:8000/employee/
```

**Detail employee**

Use the slug field. This field is the name and department of the employee concatenated in lower case, but
replacing the blanks with "_"
Use the employee name replacing the blank space for "_"
```
curl -X GET http://localhost:8000/employee/user_development/
```

**Delete employee**
```
curl -X DELETE http://localhost:8000/employee/user_development/
```

**Admin site**

You can access admin site by **http://localhost:8000/admin**