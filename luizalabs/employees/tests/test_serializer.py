import pytest
from employees.serializer import EmployeeSerializer
from employees.models import Employee


@pytest.mark.django_db
def test_total_keys_json_employee(employee):
    assert len(EmployeeSerializer(employee).data) == 3


@pytest.mark.django_db
@pytest.mark.parametrize("field", ["name", "email", "department"])
def test_keys_json_employee(employee, field):
    assert field in EmployeeSerializer(employee).data


def test_model_employee():
    assert EmployeeSerializer().Meta.model == Employee


def test_lookup_field_employee():
    assert EmployeeSerializer().Meta.lookup_field == 'slug'
