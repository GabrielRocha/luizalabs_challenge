import pytest
from employees.models import Employee


@pytest.fixture
def employee():
    return Employee.objects.create(name="Arnaldo Pereira",
                                   department="Architecture",
                                   email="arnaldo@luizalabs.com")
