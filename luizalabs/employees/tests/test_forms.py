from employees.forms import EmployeeForm
from employees.models import Employee


def test_model_of_form():
    assert EmployeeForm.Meta.model == Employee


def test_exclude_fields_form():
    form = EmployeeForm()
    assert "slug" not in form.fields.keys()
