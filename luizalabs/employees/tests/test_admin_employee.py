from employees.admin import EmployeeAdmin
from employees.models import Employee
from django.contrib import admin
import pytest


@pytest.fixture
def employee_admin():
    return EmployeeAdmin(Employee, admin.site)


def test_employee_admin_register():
    assert admin.site.is_registered(Employee)


def test_list_display_employeeadmin(employee_admin):
    assert employee_admin.list_display == ('name', 'email', 'department')


def test_empty_value_display_employeeadmin(employee_admin):
    assert employee_admin.empty_value_display == "UNKNOWN"


def test_list_filter_display_employeeadmin(employee_admin):
    assert employee_admin.list_filter == ['department']


def test_list_search_fields_employeeadmin(employee_admin):
    assert employee_admin.search_fields == ['name', 'email', 'department']
