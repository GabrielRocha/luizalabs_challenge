import pytest
from employees.models import Employee


@pytest.mark.django_db
def test_create_employee(client):
    data = {"name": "user", "email": "user@luizalab.com", "department": "Development"}
    client.post("/employee/", data=data)
    assert Employee.objects.filter(email='user@luizalab.com').exists()


@pytest.mark.django_db
@pytest.mark.usefixtures("employee")
def test_list_employees(client):
    expected = b'[{"name":"Arnaldo Pereira","email":"arnaldo@luizalabs.com","department":"Architecture"}]'
    response = client.get("/employee/")
    assert response.content == expected


@pytest.mark.django_db
@pytest.mark.usefixtures("employee")
def test_get_employees(client):
    expected = {"name": "Arnaldo Pereira", "email": "arnaldo@luizalabs.com",
                "department": "Architecture"}
    response = client.get("/employee/arnaldo_pereira_architecture/")
    assert response.json() == expected


@pytest.mark.django_db
@pytest.mark.usefixtures("employee")
def test_delete_employees(client):
    client.delete("/employee/arnaldo_pereira_architecture/")
    assert Employee.objects.filter(email='arnaldo@luizalabs.com').exists() is False
