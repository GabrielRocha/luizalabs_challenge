import pytest
from employees.models import Employee
from django.db import models


@pytest.mark.parametrize("field", ["name", "department"])
def test_charfield_employee(field):
    assert isinstance(Employee._meta.get_field(field), models.CharField)


def test_emailfield_employee():
    assert isinstance(Employee._meta.get_field("email"), models.EmailField)


@pytest.mark.django_db
@pytest.mark.usefixtures("employee")
def test_create_employee():
    assert Employee.objects.filter(email="arnaldo@luizalabs.com").exists()


@pytest.mark.django_db
@pytest.mark.usefixtures("employee")
def test_slug_field_employee():
    employee = Employee.objects.get(email="arnaldo@luizalabs.com")
    assert employee.slug == "arnaldo_pereira_architecture"


@pytest.mark.django_db
def test_str(employee):
    assert str(employee) == "Arnaldo Pereira - arnaldo@luizalabs.com"
