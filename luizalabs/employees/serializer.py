from rest_framework import serializers
from employees.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("name", "email", "department")
        model = Employee
        lookup_field = 'slug'
