from rest_framework.routers import DefaultRouter
from employees import views


router = DefaultRouter()
router.register("", views.EmployeeViewSet, 'Employee')

urlpatterns = router.urls
