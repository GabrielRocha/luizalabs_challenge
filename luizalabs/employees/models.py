from django.db import models


class Employee(models.Model):
    name = models.CharField("Name", max_length=255)
    email = models.EmailField("Email", unique=True, db_index=True)
    department = models.CharField("Department", max_length=200)
    slug = models.SlugField(null=True)

    def save(self, *args, **kwargs):
        self.slug = "_".join([self.name,
                              self.department]).lower().replace(" ", "_")
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.name} - {self.email}'
