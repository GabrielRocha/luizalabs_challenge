from django.contrib import admin
from django.contrib.auth.models import User, Group

from employees.forms import EmployeeForm
from .models import Employee


admin.site.unregister(User)
admin.site.unregister(Group)


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    form = EmployeeForm
    list_display = ('name', 'email', 'department')
    list_filter = ['department']
    empty_value_display = 'UNKNOWN'
    search_fields = ['name', 'email', 'department']
