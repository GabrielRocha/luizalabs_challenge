#!/bin/sh

set -a; . .env

until PGPASSWORD=$POSTGRES_PASSWORD psql -h postgres -U $POSTGRES_USER -d $POSTGRES_DB -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

cd luizalabs/
python manage.py migrate
if [ $1 == "test" ]
  then
    pytest --cov-report term-missing --cov . -W ignore
  else
    python manage.py $@
fi

