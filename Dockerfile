FROM python:3.6-alpine

ADD . /code

WORKDIR /code

RUN apk update && apk add --no-cache --virtual .build-dependence \
    gcc postgresql-dev tzdata python3-dev musl-dev postgresql-client\
    && ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
    && rm -rf /var/cache/apk/*

RUN pip install --no-cache-dir -r requirements/development.pip && \
    find /usr/lib/python3.*/ -name 'tests' -exec rm -r '{}' + && \
    rm -r /root/.cache

ENTRYPOINT ["/bin/sh", "run.sh"]